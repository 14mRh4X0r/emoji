package me.l4mrh4x0r.minecraft.emoji.mixin;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

@Mixin(FontRenderer.class)
public abstract class MixinFontRenderer {
    @Shadow @Final @Mutable protected byte[] glyphWidth;

    @Shadow @Final @Mutable private static ResourceLocation[] UNICODE_PAGE_LOCATIONS;

    @Shadow protected abstract void readGlyphSizes();

    /** Contains the EMP code point for the current iteration step, if any */
    @Unique
    private transient int emoji$codePoint;

    @Inject(method = "<init>", at = @At("RETURN"))
    private void onInit(CallbackInfo ci) {
        // readGlyphSizes has already been called at this point, copy existing data
        byte[] newGlyphWidth = new byte[Character.MAX_CODE_POINT + 1];
        System.arraycopy(this.glyphWidth, 0, newGlyphWidth, 0, this.glyphWidth.length);
        this.glyphWidth = newGlyphWidth;
    }

    @Inject(method = "<clinit>", at = @At("RETURN"))
    private static void onClinit(CallbackInfo ci) {
        MixinFontRenderer.UNICODE_PAGE_LOCATIONS = new ResourceLocation[(Character.MAX_CODE_POINT + 1) / 256];
    }

    @Inject(method = "onResourceManagerReload", at = @At("RETURN"))
    private void onResourceManagerReloadEnd(CallbackInfo ci) {
        this.emoji$readExtraGlyphSizes();
    }

    @Unique
    private void emoji$readExtraGlyphSizes() {
        try {
            InputStream is = Minecraft.getMinecraft()
                    .getResourceManager()
                    .getResource(new ResourceLocation("font/glyph_sizes_extra.bin"))
                    .getInputStream();
            //noinspection ResultOfMethodCallIgnored
            is.read(this.glyphWidth, Character.MIN_SUPPLEMENTARY_CODE_POINT,
                    this.glyphWidth.length - Character.MIN_SUPPLEMENTARY_CODE_POINT);
        } catch (FileNotFoundException fnfe) {
            // No extra glyph sizes found; populate with 0
            Arrays.fill(this.glyphWidth, Character.MIN_SUPPLEMENTARY_CODE_POINT, this.glyphWidth.length - 1, (byte) 0);
            LogManager.getLogger().warn("Could not load extra glyph sizes", fnfe);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("InvalidInjectorMethodSignature")
    @Inject(method = "renderStringAtPos",
            at = @At(value = "INVOKE_ASSIGN", target = "Ljava/lang/String;charAt(I)C", ordinal = 0),
            locals = LocalCapture.CAPTURE_FAILSOFT)
    private void checkSurrogate(String text, boolean shadow, CallbackInfo ci, int i, char c) {
        emoji$codePoint = c;
        if (Character.isHighSurrogate(c) && text.length() > i + 1) {
            emoji$codePoint = text.codePointAt(i);
        }
    }

    @ModifyVariable(method = "renderStringAtPos",
            at = @At(value = "INVOKE_ASSIGN", target = "Ljava/lang/String;charAt(I)C", ordinal = 0),
            ordinal = 0)
    private int increaseOffsetForSurrogate(int i) {
        return i + Character.charCount(emoji$codePoint) - 1;
    }

    @Inject(method = "renderStringAtPos", at = @At(value = "JUMP", opcode = Opcodes.IF_ICMPGE, ordinal = 0))
    private void resetCodePoint(CallbackInfo ci) {
        emoji$codePoint = 0;
    }

    @Redirect(method = "renderUnicodeChar",
            at = @At(value = "FIELD",
                    target = "Lnet/minecraft/client/gui/FontRenderer;glyphWidth:[B",
                    args = "array=get"))
    private byte getGlyphWidth(byte[] glyphWidth, int i) {
        return glyphWidth[emoji$codePoint];
    }

    @ModifyArg(method = "renderUnicodeChar",
            at = @At(value = "INVOKE",
                    target = "Lnet/minecraft/client/gui/FontRenderer;loadGlyphTexture(I)V"))
    private int loadGlyphTextureArg(int i) {
        return emoji$codePoint / 256;
    }

    @ModifyVariable(method = "renderUnicodeChar",
            at = @At(value = "STORE", ordinal = 0),
            ordinal = 2)
    private float correctXCoord(float xCoord) {
        return (emoji$codePoint % 16 * 16) + (this.glyphWidth[emoji$codePoint] >>> 4);
    }

    @ModifyVariable(method = "renderUnicodeChar",
            at = @At(value = "STORE", ordinal = 0),
            ordinal = 3)
    private float correctYCoord(float xCoord) {
        //noinspection IntegerDivisionInFloatingPointContext
        return (emoji$codePoint & 255) / 16 * 16;
    }
}
